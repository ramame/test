<?php

use yii\db\Migration;

/**
 * Handles the creation of table `urgency`.
 */
class m180624_053756_create_urgency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('urgency', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
 ]);
        $this->insert('urgency', [
            'name' => 'low',
  ]);
          $this->insert('urgency', [
            'name' => 'normal',
  ]);
          $this->insert('urgency', [
            'name' => 'critical',
  ]);
     
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('urgency');
    }
}
